import threading
import win32com.client
from bound_driectory import BoundDirectory


__author__ = 'shachar'


class Bounder(threading.Thread):
    """
        Make a Thread which copy pictures from the cache to dst_path
    """
    def __init__(self, src_path, dst_path, formats, **kwds):
        threading.Thread.__init__(self, **kwds)
        self.bound_directory = BoundDirectory(src_path, dst_path, formats)

    def run(self):
        self.bound_directory.start()


def search_google(search_query):
    """
        Search in Google Pictures for the given query
    """
    url               = "http://www.google.com/search?q={SEARCH_QUERY}&tbm=isch".format(SEARCH_QUERY=search_query)
    internet_explorer = win32com.client.Dispatch("InternetExplorer.Application")
    internet_explorer.visible = 0   # So the window will not be opened
    internet_explorer.navigate(url)

if __name__ == "__main__":
    src_path = "C:\Users\{USERNAME}\AppData\Local\Microsoft\Windows\Temporary Internet Files".format(USERNAME="shachar")
    dst_path = "C:\Users\{USERNAME}\Desktop\pictures".format(USERNAME="shachar")
    formats  = [".jpg", ".png", ".bmp"]
    bounder  = Bounder(src_path, dst_path, formats)
    bounder.start()

    # Stop running of main and Thread when '<Ctrl> + C' is pressed
    try:
        while bounder.bound_directory.run:
            query = raw_input("Please Enter a query to search for in 'Google Pictures': ")
            search_google(query)
    except KeyboardInterrupt:
        bounder.bound_directory.run = False

