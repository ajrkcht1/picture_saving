from __future__ import generators   # For previous versions of Pythons (using 'yield')
import os
import win32file
import win32con
import shutil

__author__          = 'user'
FILE_LIST_DIRECTORY = 0x0001


class BoundDirectory(object):
    #------Constant Variables------
    created_value       = 1
    file_list_directory = FILE_LIST_DIRECTORY
    #------------------------------

    def __init__(self, src_path, dst_path, formats, include_subdirectories=True, run=False):
        self.src_path               = src_path
        self.dst_path               = dst_path
        self.formats                = formats[:]
        self.include_subdirectories = include_subdirectories
        self.run                    = run

    def get_created_files(self):
        """
            Yield full-path of files which been added to the src-folder
        """
        handle_dir = win32file.CreateFile(
            self.src_path,
            BoundDirectory.file_list_directory,
            win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE | win32con.FILE_SHARE_DELETE,
            None,
            win32con.OPEN_EXISTING,
            win32con.FILE_FLAG_BACKUP_SEMANTICS,
            None
)
        while self.run:
            results = win32file.ReadDirectoryChangesW(
                handle_dir,
                win32file.GetFileSize(handle_dir),
                self.include_subdirectories,
                win32con.FILE_NOTIFY_CHANGE_FILE_NAME,
                None,
                None
            )
            for action, filename in results:
                full_filename = os.path.join(self.src_path, filename)
                if action == BoundDirectory.created_value and os.path.exists(full_filename) and not os.path.isdir(full_filename) and os.path.splitext(full_filename)[-1] in self.formats:
                    yield full_filename

    def copy(self):
        """
            Copy files from the cache to dst_path which been added
        """
        for path_file in self.get_created_files():
            shutil.copy2(path_file, self.dst_path)

    def start(self):
        """
            Start the program
        """
        self.run = True
        self.copy()

#   An example of usage:
if __name__ == "__main__":
    src_path = "C:\\Users\\{USERNAME}\\AppData\\Local\\Microsoft\\Windows\\Temporary Internet Files".format(USERNAME="shachar")
    dst_path = "C:\\Users\\{USERNAME}\\desktop\\pictures\\".format(USERNAME="shachar")
    formats  = [".jpg", ".png", ".bmp"]
    bound_directory = BoundDirectory(src_path, dst_path, formats)
    bound_directory.start()